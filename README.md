README
======
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Hindi, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Hindi data result from an update of the Hindi part of the [PARSEME 1.2 corpus](http://hdl.handle.net/11234/1-3367).
For the changes with respect to the 1.2 version, see the change log below.

The raw corpus, release with version 1.2, can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Corpora
-------
All annotated data come from the Test section of the [Hindi Treebank](http://ltrc.iiit.ac.in/treebank_H2014), which is newswire text. 
All raw corpora are a portion of the [HindiMonocorp](https://lindat.mff.cuni.cz/repository/xmlui/handle/11858/00-097C-0000-0023-6260-A) which has been scraped from the web.


Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. The following tagsets are used:
* column 3 (LEMMA): Available. Automatically generated (UDPipe 2, model hindi-hdtb-ud-2.10-220711).
* UPOS (column 4): Available. Automatically generated (UDPipe 2, model hindi-hdtb-ud-2.10-220711). The tagset is the one of [UD POS-tags](http://universaldependencies.org/u/pos).
* XPOS (column 5): Available. Automatically generated (UDPipe 2, model hindi-hdtb-ud-2.10-220711). The tagset is probably the one of Ancora.
* FEATS (column 6): Available. Automatically generated (UDPipe 2, model hindi-hdtb-ud-2.10-220711). The tagset is [UD features](http://universaldependencies.org/u/feat/index.html).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically generated (UDPipe 2, model hindi-hdtb-ud-2.10-220711). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* MISC (column 10): No-space information available for Ancora (automatically annotated), unavailable for the UD corpora.
* PARSEME:MWE (column 11): Manually annotated. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: LVC.cause, LVC.full, MVC, VID.

Tokenization
------------
* Hyphens: Split as a single token.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Licence
-------
The annotated data in column 11 are licensed under **Creative Commons Non-Commercial Share-Alike 4.0** licence [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Authors
-------
* Archna Bhatia
* Ashwini Vaidya

Contact
-------
* abhatia@ihmc.org
* avaidya@hss.iitd.ac.in

Future work
-------
The character encoding may be problematic. Namely, after re-annotating morphosyntax with UDPipe 2.10, the encoding of some characters is not the same after as before the re-annotation.
See the logs and screenshots in not_to_release/UDPIPE-REANNOTATION. This problem should be checked and fixed in new releases as it may have consequences for NLP tools.

Change log
-------

- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - The morphosyntactic annotation (columns LEMMA to MISC) from version 1.2 was re-generated with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2/) (hindi-hdtb-ud-2.10-220711 model) by Agata Savary.
- **2020-07-09**:
  - [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.

